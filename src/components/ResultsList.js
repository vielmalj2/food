import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import {withNavigation} from 'react-navigation'
import ResultsDetails from './ResultsDetails';

const ResultsList = ({ title, results, navigation }) => {

    if (!results.length) {
        return null;
    }
    
    return (
    <View style ={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity>
           <Text style= {styles.textGit}>Prueba de git</Text>
        </TouchableOpacity>
        <TouchableOpacity>
           <Text style= {styles.textGit2}>Prueba de git 2</Text>
        </TouchableOpacity>
        <TouchableOpacity>
           <Text style= {{color: 'red'}}>Prueba de react 3000</Text>
        </TouchableOpacity>

        <FlatList
           showsHorizontalScrollIndicator = {false}
           horizontal
           data = {results}
           keyExtractor = {result => result.id}
           renderItem = {({ item }) => {
           return (
               <TouchableOpacity onPress={() => navigation.navigate ('ResultsShow' , {id: item.id})}>
                  <ResultsDetails result ={item}/>
               </TouchableOpacity>
           )
           }}
        />
    </View>
    );
};

const styles = StyleSheet.create ({
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 5
    },
    container: {
        marginBottom: 10
    },
    textGit: {
        fontSize: 8,
        marginBottom: 15,
        backgroundColor: 'blue'
    },
    textGit2: {
        fontSize: 14,
        marginBottom: 15,
        backgroundColor: 'green'
    }
});

export default withNavigation (ResultsList);