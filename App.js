import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SearchScreen from './src/screens/SearchScreen';
import ResultsShowScreen from './src/screens/ResultsShowScreen'
const navigator = createStackNavigator(
{
  Search: SearchScreen,
  ResultsShow: ResultsShowScreen
},
{       // Al navegador de pila le agregamos dos atributos
  initialRouteName: 'Search',    // este me determina cual sera la primera vista de la app
  defaultNavigationOptions: {    // con esta personalizo el encabezado que se muestra n la parte superior de cada pantalla
    title: 'Business Search'
  }
}
);

export default createAppContainer (navigator);